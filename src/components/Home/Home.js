import React from 'react';
import PropTypes from 'prop-types';
import './Home.css';

const Home = (props) => (
  <div className="Home">
    <h1>tu color aleatorio es: </h1>
    <p> app engine standard environment: <a href={'https://standar-app.appspot.com'}> https://standar-app.appspot.com </a> </p>
  </div>
);

Home.propTypes = {
  counter: PropTypes.number.isRequired,
  increment: PropTypes.func.isRequired
};

export default Home;
