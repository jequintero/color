import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

import './App.css';

class App extends Component {
  state = { color: '#fff' };
  componentDidMount() {
    axios.get(`https://standar-app.appspot.com`)
      .then(res => {
        console.log(res);
        this.setState({ color: res.data.color });
      });
  }
  render() {
    return (
      <section style={{ background: this.state.color, height: '100vh', padding: '0' }}>
        <div class={'content'}>{this.props.children}</div>
      </section>
    );
  }
}


App.propTypes = {
  children: PropTypes.any
};

export default App;
